import React, {useState, useEffect, useContext} from 'react';
//import react bootstap components
import Container from 'react-bootstrap/Container'

//importing components
// import Course from './../components/Course';
import AdminView from './../components/AdminView';
import UserView from './../components/UserView';

// // importing mock-data
// import courses from './../mock-data/courses';

//import contet
import UserContext from './../UserContext';

export default function Courses () {

	const [courses, setCourses] = useState([]);
	const {user, setUser} = useContext(UserContext);

	const fetchData = () => {

		let token = localStorage.getItem("token");
		fetch('http://localhost:4000/api/courses/getAllCourses',
			
				{
					method: "GET",
					headers: 
						{
						"Authorization": `Bearer ${token}`
						}
				}).then( result => result.json())
				.then( result => {
					console.log(result)
					setCourses(result)
		})
	}

	useEffect (()=>{
		fetchData()
	},[])

	// let CourseCards =  result.map( (course)=> {
	// 					return <Course key={course.id} course={course}/> 
	// 			})
	
	

	return (
		<Container className="p-4">
			{ (user.isAdmin === true)?
			<AdminView courseData={courses} fetchData={fetchData}/>
			:
			<UserView courseData={courses}/>
			}
		</Container>)
}

