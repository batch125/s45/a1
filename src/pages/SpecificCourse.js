import React, {useContext, useEffect, useState} from 'react';

import UserContext from './../UserContext';

import {Link, useParams, useHistory} from 'react-router-dom';

import {Container, Card, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';


export default function SpecificCourse(){

	const [courseName, setCourseName] = useState('');
	const [courseDesc, setCourseDesc] = useState('');
	const [price, setPrice] = useState(0)

	const { user } = useContext(UserContext);

	const { courseId } = useParams();

	let token = localStorage.getItem('token')

	let history = useHistory();

	useEffect( () => {
		fetch(`http://localhost:4000/api/courses/${courseId}`,
			{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setCourseName(result.courseName);
			setCourseDesc(result.courseDesc);
			setPrice(result.price);
		})
	}, [])

	const enroll = () => {
		fetch('http://localhost:4000/api/users/enroll', 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			}
		)
		.then(result => result.json())
		.then(result => {
			console.log(result)

			if(result === true){

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Enrolled Successfully" 
				})

				history.push('/courses');
			} else {
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: "Something went wrong" 
				})
			}
		})
	}

	return(
		<Container className="mt-3">
			<Card>
				<Card.Header>
					<h4>
						{/*course name*/}
						{courseName}
					</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>
						{/*course description*/}
						{courseDesc}
					</Card.Text>
					<h6>
						Price: Php 
						{/*course price*/}
						<span className="mx-2">{price}</span>
					</h6>
				</Card.Body>
				<Card.Footer>
					{
						(user.id !== null) ?
								<Button variant="primary" 
								onClick={ () => enroll() }

								> Enroll</Button>
							:
								<Link className="btn btn-danger" to="/login">Login to Enroll</Link>
					}
				</Card.Footer>
			</Card>
		</Container>
	)
}
