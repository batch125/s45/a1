import React, {useState, useEffect, useContext} from 'react';

import {Redirect,useHistory} from 'react-router-dom'

//import contet
import UserContext from './../UserContext';

//import react boostrap
import {Container, Form, Button} from 'react-bootstrap'


import Swal from 'sweetalert2';



export default function Register(){

	const [firstName,setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo,setMobileNo] = useState('');
	const [email,setEmail] = useState('');
	const [password,setPassword] = useState('');
	const [VerifyPassword,setVerifyPassword] = useState('');
	const [isDisabled,setIsDisabled] = useState(true);

	// destructure context object
	const {user, setUser, unsetUser} = useContext(UserContext);

	let history = useHistory();

	useEffect (()=>{
		if (email !=='' && password !=='' && VerifyPassword!=='' && password===VerifyPassword) {
			{setIsDisabled(false)}
		}
		else {
			setIsDisabled(true)
		}	
	},[email,password,VerifyPassword]);

	function register(e){
		e.preventDefault();

	fetch('http://localhost:4000/api/users/checkEmail', {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then( result => result.json())
			.then( result => {
				// console.log(result)	//boolean

				if(result === true){
					// alert("User already exist")
					Swal.fire({
						title: 'Duplicate email found',
						icon: 'error',
						text: 'Please choose another email'
					})
				} else {
					//what to do when user/email still not existing?

					fetch('http://localhost:4000/api/users/register', {
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password
						})
					})
					.then( result => result.json())
					.then( result => {
						//console.log(result)	//boolean

						if(result === true){
							Swal.fire({
								title: "Registration Successful",
								icon: "success",
								text: "Please proceed to login"
							})
							history.push('/login');
						} 
						else {
								Swal.fire({
									title: 'Something went wrong',
									icon: 'error',
									text: 'Please try again'
							})
						}
					})
				}
			})

		setEmail('');
		setPassword('');
		setVerifyPassword('');
	}

	return(
		(user.id !== null)?
			<Redirect to="/"/>
			:
			<Container className="mb-3 p-3 justify-content-center">
				<h1>Register</h1>
				<Form onSubmit={(e)=> register(e)}>
				 <Form.Group className="mb-3" controlId="formlirstName">
				    <Form.Label>First Name</Form.Label>
				    <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={(e)=> setFirstName(e.target.value)}/>
				  </Form.Group>
				  <Form.Group className="mb-3" controlId="formlastName">
				    <Form.Label>Last Name</Form.Label>
				    <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={(e)=> setLastName(e.target.value)}/>
				  </Form.Group>
				  <Form.Group className="mb-3" controlId="formmobileNo">
				    <Form.Label>Mobile Number</Form.Label>
				    <Form.Control type="text" placeholder="Enter Mobile Number" value={mobileNo} onChange={(e)=> setMobileNo(e.target.value)}/>
				  </Form.Group>
				  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e)=> setEmail(e.target.value)}/>
				  </Form.Group>
				  <Form.Group className="mb-3" controlId="formBasicPassword">
				    <Form.Label>Password</Form.Label>
				    <Form.Control type="password" placeholder="Password" value={password} onChange={(e)=>  setPassword(e.target.value)}/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="formVerifyPassword">
				    <Form.Label>Verify Password{}</Form.Label>
				    <Form.Control type="password" placeholder="Verify Password" value={VerifyPassword} onChange={(e)=> setVerifyPassword(e.target.value)}/>
				  </Form.Group>
				  <Button variant="primary" type="submit" disabled={isDisabled}>
				    Submit
				  </Button>
				</Form>
			</Container>
		)
}