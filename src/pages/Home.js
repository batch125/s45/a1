import React from 'react';

//import react bootstap components
import Container from 'react-bootstrap/Container'

//import components
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

export default function Home() {

	return (
		<Container fluid>
			<Banner/>
			<Highlights/>
		</Container>
		)
}
