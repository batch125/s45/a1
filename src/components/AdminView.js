import React, {useState, useEffect, Fragment} from 'react'

import {Container, Table, Button, Modal, Form} from 'react-bootstrap'

import Swal from 'sweetalert2'



export default function AdminView(props){
	console.log(props)

	const { courseData, fetchData } = props;
	console.log(courseData) //array of courses

	const [courseId, setCourseId] = useState('');
	const [courses, setCourses] = useState([]);
	const [courseName, setCourseName] = useState('');
	const [courseDesc, setCourseDesc] = useState('');
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false)

	let token = localStorage.getItem('token')

	const openAdd = () => setShowAdd (true);
	const closeAdd =() => setShowAdd(false)

	

	const openEdit = (courseId) => {
		fetch(`http://localhost:4000/api/courses/${courseId}`,{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setCourseId(result._id);
			setCourseName(result.courseName);
			setCourseDesc(result.courseDesc);
			setPrice(result.price)
		})

		setShowEdit(true);
	}

	const closeEdit = () => {

		setShowEdit(false);
		setCourseName('');
		setCourseDesc('');
		setPrice(0);
	}

	useEffect( () => {
		const coursesArr = courseData.map( (course) => {
			console.log(course)
			return(
				<tr key={course._id}>
					<td>{course.courseName}</td>
					<td>{course.courseDesc}</td>
					<td>{course.price}</td>
					<td>
						{
							(course.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
					<td>
						<Fragment>
							<Button variant="primary" size="sm" 
							onClick={ ()=> openEdit(course._id) }>
								Update
							</Button>
							<Button variant="danger" size="sm"
							onClick={ () => deleteToggle(course._id)}>
								Delete
							</Button>
						</Fragment>


						{
							(course.isActive === true) ?
								<Button variant="warning" size="sm"
								onClick={()=> archiveToggle(course._id, course.isActive)}>
									Disable
								</Button>
							:
								<Button variant="success" size="sm"
								onClick={ () => unarchiveToggle(course._id, course.isActive)}>
									Enable
								</Button>
						}
					
					</td>
				</tr>
			)
		})

		setCourses(coursesArr)
	}, [courseData])

	// edit course function
	const editCourse = (e) =>{

		e.preventDefault()

		fetch(`http://localhost:4000/api/courses/${courseId}/edit`,
		{
			method:"PUT",
			headers: {
				"Content-Type":"application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				courseName:courseName,
				courseDesc:courseDesc,
				price: price
			})
		})
		.then(result => result.json())
		.then(result =>{
			console.log(result)

			fetchData()

			if (typeof result !== "undefined"){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Course successfuly updated"
				})
				closeEdit()
			} else {
				fetchData()
				Swal.fire({
						title: "Failed",
						icon: "error",
						text: "Something went wrong"
					})
				closeEdit()
			}
		})
	}

	// update course
	const archiveToggle = (courseId, isActive) =>{

		fetch(`http://localhost:4000/api/courses/${courseId}/archive`,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					isActive:isActive
				})
			}
			)
			.then(result => result.json())
			.then( result => {
				console.log(result)

				fetchData();
				if(result === true) 
				{
					Swal.fire({
						title:"Success",
						icon:"success",
						text:"Course status updated"
					})
				}
				else {
					fetchData();
					Swal.fire({
						title:"Success",
						icon:"error",
						text:"Something went wrong"
					})
				}
			})
	}

	const unarchiveToggle = (courseId, isActive) => {

		fetch(`http://localhost:4000/api/courses/${courseId}/unarchive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			fetchData();
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					"text": "Course status successfully updated"
				})
			} else {
				fetchData();
				Swal.fire({
					title: "Failed",
					icon: "error",
					"text": "Something went wrong"
				})
			}
		})
	}

	const deleteToggle = (courseId) => {
		fetch(`http://localhost:4000/api/courses/${courseId}/delete`, {
			method: "DELETE",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			fetchData();
			if(result === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					"text": "Course status successfully updated"
				})
			} else {
				fetchData();
				Swal.fire({
					title: "Failed",
					icon: "error",
					"text": "Something went wrong"
				})
			}
		})
	}

	const addCourse = (e) => {
		e.preventDefault()

		fetch(`http://localhost:4000/api/courses/addCourse`,
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					courseName: courseName,
					courseDesc: courseDesc,
					price: price
				})
			}).then(result=> result.json())
			.then(result => {
				console.log(result)

				if (result === true) 
					{
					fetchData()

					Swal.fire({
						title:"Success",
						icon:"success",
						text:"Course successfuly added"
					})

					setCourseName('');
					setCourseDesc('');
					setPrice(0);

					closeAdd();
				} 
				else {
					fetchData()
					Swal.fire({
						title:"Failed",
						icon:"error",
						text:"Something went wrong"
					})
				}

			})
	}

	return(
		<Container>
			<div>
				<h2 className="text-center">Admin Dashboard</h2>
				<div className="d-flex justify-content-end mb-3">
					<Button variant="primary" onClick={openAdd}>Add New Course</Button>
				</div>
			</div>

			<Table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{/*display the courses*/}
					{courses}
				</tbody>
			</Table>
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(e) => editCourse(e, courseId)} >
					<Modal.Header>
						<Modal.Title>Edit Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="courseName" >
							<Form.Label>Name</Form.Label>
							<Form.Control
								type="text"
								value={courseName}
								onChange={(e) => setCourseName(e.target.value)} 
								/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								type="text"
								value={courseDesc}
								onChange={(e) => setCourseDesc(e.target.value)} 
								/>
						</Form.Group>
						<Form.Group controlId="coursePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={(e) => setPrice(e.target.value)} 
								/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>					
					</Modal.Footer>
				</Form>
			</Modal>
			{/*Add course Modal*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={(e)=> addCourse(e)}>
					<Modal.Header>
						<Modal.Title>Edit Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group controlId="courseName" >
							<Form.Label>Name</Form.Label>
							<Form.Control
								type="text"
								value={courseName}
								onChange={(e) => setCourseName(e.target.value)} 
								/>
						</Form.Group>
						<Form.Group controlId="courseDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								type="text"
								value={courseDesc}
								onChange={(e) => setCourseDesc(e.target.value)} 
								/>
						</Form.Group>
						<Form.Group controlId="coursePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								type="number"
								value={price}
								onChange={(e) => setPrice(e.target.value)} 
								/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>					
					</Modal.Footer>
				</Form>
			</Modal>
		</Container>
	)
}