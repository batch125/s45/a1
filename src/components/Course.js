import React, {useState, useEffect} from 'react'
import PropTypes from 'prop-types';

import {Link} from 'react-router-dom'


//importing react-bootstrap
import {Card, Button} from 'react-bootstrap';

export default function Course({courseProp}){
	console.log(courseProp)
	const {courseName, courseDesc, price, _id} = courseProp
/*	

	
	console.log(courseName)

	//return

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10)
	const [isDisabled, setIsDisabled] = useState (false)

	function enroll(){
		if (seats > 0) {
			setCount(count+1)
			setSeats(seats-1)
		}
		// else 
		// {
		// 	alert(`No slot available`)
		// }
	}

	useEffect (()=>{

		if (seats===0)
				setIsDisabled(true)
			},[seats]);
*/
	return (
		
				<Card className="mb-3">
				  <Card.Body>
				    <Card.Title>{courseName}</Card.Title>
					    <h6>Description</h6>
					    <p>{courseDesc}</p>
					    <h6>Price:</h6>
					    <p>{price}</p>
					    {/*<h5>Enrollees:</h5>*/}
					    {/*<h5>Enrollees</h5>
	    				<p>{count} Enrollees</p>
	    				<h5>Seats</h5>
	    				<p>{seat} Seats</p>
	    		    	<Button variant="primary" onClick={enroll} disabled={isDisabled}>Enroll</Button>*/}
	    		    <Link className="btn btn-primary" to={`/courses/${_id}`}>
				    	Details
				    </Link>
				  </Card.Body>
				</Card>

		)
}

Course.propTypes = {
	course: PropTypes.shape({
	courseName: PropTypes.string.isRequired,
		courseDesc: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
	})
}