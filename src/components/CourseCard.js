import React from 'react'

//import react bootstap components
import {Col,Row, Card, Button} from 'react-bootstrap';

export default function CourseCard() {

	return (

			<Row className ="px-4 m-5">
				<Col xs={12} md={4}>
					<Card>
					  <Card.Body>
					    <Card.Title>Sample Course</Card.Title>
					    <Card.Text>
						     Sample Description
						     <br/>
						     This is a sample course offering
					    </Card.Text>
				        <Card.Text>
				    	     Price:
				    	     <br/>
				    	     Php 40,000
				        </Card.Text>
					    <Button variant="primary">Enroll</Button>
					  </Card.Body>
					</Card>
				</Col>

			</Row>
		)

}

