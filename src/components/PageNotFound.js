import React from 'react';

import {Link} from "react-router-dom";

import {Container,Row, Col, Jumbotron} from 'react-bootstrap';

export default function PageNotFound(){

  return (
   
    <Container fluid>
      <Row>
        <Col className="px-0">
          <Jumbotron fluid className="px-3">
            <h1>404</h1>
            <p>
             Page not Found
            </p>  
            <Link to="/">Go Back to Home</Link>
          </Jumbotron>  
        </Col>
      </Row>
    </Container>

    )
}