import React from 'react'

//import react bootstap components
import {Col,Row, Card} from 'react-bootstrap';

export default function Highlights() {

	return (
			<Row className ="px-4 m-5">
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Learn from Home</Card.Title>
				    <Card.Text>
					     Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident perspiciatis adipisci accusamus animi fugiat, similique, minus odit alias voluptatem blanditiis velit cupiditate voluptatibus dicta expedita consequuntur nobis ullam magni quae?
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
			<Card>
			  <Card.Body>
			    <Card.Title>Study Now, Pay Later</Card.Title>
			    <Card.Text>
				     Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident perspiciatis adipisci accusamus animi fugiat, similique, minus odit alias voluptatem blanditiis velit cupiditate voluptatibus dicta expedita consequuntur nobis ullam magni quae?
			    </Card.Text>
			    
			  </Card.Body>
			</Card>
			</Col>

			<Col xs={12} md={4}>
			<Card>
			  <Card.Body>
			    <Card.Title>Be Part of Our Community</Card.Title>
			    <Card.Text>
				     Lorem ipsum dolor, sit amet consectetur adipisicing elit. Provident perspiciatis adipisci accusamus animi fugiat, similique, minus odit alias voluptatem blanditiis velit cupiditate voluptatibus dicta expedita consequuntur nobis ullam magni quae?
			    </Card.Text>
			    
			  </Card.Body>
			</Card>
			</Col>


			</Row>
		)

}

